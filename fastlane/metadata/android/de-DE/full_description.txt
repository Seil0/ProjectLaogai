ProjectLaogai ist eine App um den Vorlesungsplan, die Notenverwaltung und den Mensa-Speiseplan der Hochschule Offenburg anzuzeigen.

Features:
* lass dir deine Noten direkt in der App anzeigen
* zeige den Stundenplan deines Studiengangs an
* schau dir den Mensa Speiseplan der aktuellen und der nächsten Woche an
* schaue dir das aktuelle Guthaben deiner Mensakarte an
* öffne moodle direkt in der App

Bitte melde Fehler und Probleme an support@mosad.xyz
