Version 0.6.1 "anthropomorphised Athena"

* Grades can be synced in the background
* Grades will be cached for offline use or if qispos is not reachable
* The message "Menu empty" is no longer displayed if there is a menu in the next week
