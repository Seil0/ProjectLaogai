This release 0.5.0 is called  "artistical Apollon".

* new: it's now possible to choose a theme (light, dark or black)
* new: you can now check your current mensa card balance
* change: updated some libs, updated kotlin to 1.3.41
* change: added a license dialog for all used libraries
* fix: the mensa should now show the correct meals on sunday/monday
* fix: the timetable should now show the correct on the sunday/monday change
