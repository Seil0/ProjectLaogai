/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mosad.seil0.projectlaogai.controller.GradesController
import org.mosad.seil0.projectlaogai.util.GradeSubject
import java.io.File
import java.io.FileReader

class GradesControllerTest {

    @Test
    fun diffGrades_noDiff() {
        val origFile = File(GradesControllerTest::class.java.getResource("/grades_orig.json")?.path!!)
        val diffFile = File(GradesControllerTest::class.java.getResource("/grades_orig.json")?.path!!)

        val mapA: HashMap<String, ArrayList<GradeSubject>> = FileReader(origFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val mapB: HashMap<String, ArrayList<GradeSubject>> = FileReader(diffFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val exp = ArrayList<GradeSubject>()
        val ret = GradesController().diffGrades(mapA, mapB)

        Assertions.assertEquals(exp, ret)
    }

    @Test
    fun diffGrades_diffSubject() {
        val origFile = File(GradesControllerTest::class.java.getResource("/grades_orig.json")?.path!!)
        val diffFile = File(GradesControllerTest::class.java.getResource("/grades_diff_subject.json")?.path!!)

        val mapA: HashMap<String, ArrayList<GradeSubject>> = FileReader(origFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val mapB: HashMap<String, ArrayList<GradeSubject>> = FileReader(diffFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val exp = arrayListOf(
            GradeSubject("AI-3010", "Computernetze", "WiSe 18/19", "0,7", "2,0"),
            GradeSubject("AI-3020", "Datenbanksysteme 1", "WiSe 18/19", "1,7", "2,0"),
            GradeSubject("AI-3025", "Praktikum Datenbanksysteme", "WiSe 18/19", "bestanden", "3,0")
        )
        val ret = GradesController().diffGrades(mapA, mapB)

        Assertions.assertEquals(exp, ret)
    }

    @Test
    fun diffGrades_diffSemester() {
        val origFile = File(GradesControllerTest::class.java.getResource("/grades_orig.json")?.path!!)
        val diffFile = File(GradesControllerTest::class.java.getResource("/grades_diff_semester.json")?.path!!)

        val mapA: HashMap<String, ArrayList<GradeSubject>> = FileReader(origFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val mapB: HashMap<String, ArrayList<GradeSubject>> = FileReader(diffFile).use {
            GsonBuilder().create().fromJson(it, object : TypeToken<HashMap<String, ArrayList<GradeSubject>>>() {}.type)
        }

        val exp = arrayListOf(
            GradeSubject("AI-2010", "Mathemaik 7", "SoSe 19", "1,7", "4,0"),
            GradeSubject("AI-2015", "Praktikum Mathemaik 7", "SoSe 19", "bestanden", "1,0")
        )
        val ret = GradesController().diffGrades(mapA, mapB)

        Assertions.assertEquals(exp, ret)
    }
}