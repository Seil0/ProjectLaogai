package org.mosad.seil0.projectlaogai.uicomponents.dialogs

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import org.mosad.seil0.projectlaogai.R

class LoadingDialog(val context: Context) {

    private val dialog = MaterialDialog(context)

    init {
        dialog.cancelable(false)
            .cancelOnTouchOutside(false)
            .customView(R.layout.dialog_loading)
    }

    fun show() {
        dialog.show()
    }

    fun show(func: LoadingDialog.() -> Unit): LoadingDialog = apply {
        func()
        this.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }
}