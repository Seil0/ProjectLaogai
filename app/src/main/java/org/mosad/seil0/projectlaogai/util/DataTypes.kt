/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.util

import android.graphics.Color
import kotlin.collections.ArrayList

class DataTypes {
    val times = arrayOf("8.00 - 9.30", "9.45 - 11.15", "11.35 - 13.05", "14.00 -15.30", "15.45 - 17.15", "17.30 - 19.00")

    val primaryColors = intArrayOf(
        Color.parseColor("#E53935"),
        Color.parseColor("#E91E63"),
        Color.parseColor("#9C27B0"),
        Color.parseColor("#673AB7"),
        Color.parseColor("#3F51B5"),
        Color.parseColor("#2196F3"),
        Color.parseColor("#03A9F4"),
        Color.parseColor("#00BCD4"),
        Color.parseColor("#009688"),
        Color.parseColor("#4CAF50"),
        Color.parseColor("#8BC34A"),
        Color.parseColor("#CDDC39"),
        Color.parseColor("#FDD835"),
        Color.parseColor("#FFB300"),
        Color.parseColor("#FB8C00"),
        Color.parseColor("#FF5722"),
        Color.parseColor("#795548"),
        Color.parseColor("#9E9E9E"),
        Color.parseColor("#607B8B"),
        Color.parseColor("#000000")
    )

    val accentColors = intArrayOf(
        Color.parseColor("#FF1744"),
        Color.parseColor("#F50057"),
        Color.parseColor("#D500F9"),
        Color.parseColor("#3F51B5"),
        Color.parseColor("#3D5AFE"),
        Color.parseColor("#2979FF"),
        Color.parseColor("#0096FF"),
        Color.parseColor("#00E5FF"),
        Color.parseColor("#1DE9B6"),
        Color.parseColor("#00E676"),
        Color.parseColor("#C6FF00"),
        Color.parseColor("#FFD600"),
        Color.parseColor("#FFC400"),
        Color.parseColor("#FF9100"),
        Color.parseColor("#FF3D00"),
        Color.parseColor("#000000")
    )

    enum class Theme(var string: String) {
        Light("Light"),
        Dark("Dark"),
        Black("Black")
    }
}

// data classes for the course part
data class Course(val courseLink: String, val courseName: String)

data class CoursesMeta(val updateTime: Long = 0, val totalCourses: Int = 0)

data class CoursesList(val meta: CoursesMeta = CoursesMeta(), val courses: ArrayList<Course> = ArrayList())

// data classes for the Mensa part
data class Meal(val day: String, val heading: String, val parts: ArrayList<String>, val additives: String)

data class Meals(val meals: ArrayList<Meal>)

data class MensaWeek(val days: Array<Meals> = Array(7) { Meals(ArrayList()) })

data class MensaMeta(val updateTime: Long = 0, val mensaName: String = "")

data class MensaMenu(val meta: MensaMeta = MensaMeta(), val currentWeek: MensaWeek = MensaWeek(), val nextWeek: MensaWeek = MensaWeek())

// data classes for the timetable part
data class Lesson(
    val lessonID: String,
    val lessonSubject: String,
    val lessonTeacher: String,
    val lessonRoom: String,
    val lessonRemark: String
)

data class TimetableDay(val timeslots: Array<ArrayList<Lesson>> = Array(6) { ArrayList() })

data class TimetableWeek(val weekIndex: Int = 0, val weekNumberYear: Int = 0, val days: Array<TimetableDay> = Array(6) { TimetableDay() })

// data classes for the qispos part
data class GradeSubject(val id: String = "", val name: String = "", val semester: String = "", val grade: String = "", val credits: String = "")

// TCoR
data class TimetableWeekTCoR(val days: Array<TimetableDay> = Array(6) { TimetableDay() })

data class TimetableCourseMeta(val updateTime: Long = 0, val courseName: String = "", val weekIndex: Int = 0, val weekNumberYear: Int = 0, val link: String = "")

data class TimetableCourseWeek(val meta: TimetableCourseMeta = TimetableCourseMeta(), var timetable: TimetableWeekTCoR = TimetableWeekTCoR())
