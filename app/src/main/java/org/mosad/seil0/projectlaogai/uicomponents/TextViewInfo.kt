/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.uicomponents

import android.content.Context
import android.graphics.Rect
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import org.mosad.seil0.projectlaogai.R


class TextViewInfo(context: Context?): AppCompatTextView(context!!) {

    var txt = ""
    var txtSize = 15F
    var txtAlignment = View.TEXT_ALIGNMENT_CENTER
    var drawable = R.drawable.ic_error_outline_black_24dp
    var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

    init {
        params.setMargins(0,200,0,0)

    }

    fun set(func: TextViewInfo.() -> Unit): TextViewInfo = apply {
        func()

        text = txt
        layoutParams = params
        textSize = txtSize
        textAlignment = txtAlignment
    }

    fun showImage(): TextViewInfo = apply {
        val img = ResourcesCompat.getDrawable(resources, drawable, context.theme)?.apply {
            bounds = Rect(0, 0, 75, 75)
        }
        setCompoundDrawables(null, null, null, img)
    }

}