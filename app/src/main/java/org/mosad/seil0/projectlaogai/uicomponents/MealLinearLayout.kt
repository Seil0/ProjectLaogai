/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.uicomponents

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import org.mosad.seil0.projectlaogai.databinding.LinearlayoutMealBinding
import org.mosad.seil0.projectlaogai.util.Meal

class MealLinearLayout(context: Context?): LinearLayout(context) {

    private val binding = LinearlayoutMealBinding.inflate(LayoutInflater.from(context), this, true)

    fun setMeal(meal: Meal) {
        binding.textMealHeading.text = meal.heading

        meal.parts.forEachIndexed { partIndex, part ->
            binding.textMeal.append(part)
            if(partIndex < (meal.parts.size - 1))
                binding.textMeal.append("\n")
        }
    }

    fun disableDivider() {
        binding.dividerMeal.visibility = View.GONE
    }

}