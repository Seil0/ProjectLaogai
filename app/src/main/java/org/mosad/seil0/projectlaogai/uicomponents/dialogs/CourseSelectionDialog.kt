package org.mosad.seil0.projectlaogai.uicomponents.dialogs

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.list.listItems
import org.mosad.seil0.projectlaogai.R

class CourseSelectionDialog(val context: Context) {

    private val dialog = MaterialDialog(context)

    var list: List<String> = listOf()
    var selectedIndex = 0

    init {
        dialog.title(R.string.select_course)
    }

    fun show() {
        dialog.show()
    }

    fun show(func: CourseSelectionDialog.() -> Unit): CourseSelectionDialog = apply {
        func()
        this.show()
    }

    @Suppress("unused")
    fun dismiss() {
        dialog.dismiss()
    }

    fun onDismiss(func: CourseSelectionDialog.() -> Unit): CourseSelectionDialog = apply {
        dialog.onDismiss {
            func()
        }
    }

    fun listItems(func: CourseSelectionDialog.() -> Unit): CourseSelectionDialog = apply {
        dialog.listItems(items = list) { _, index, _ ->
            selectedIndex = index

            func()
        }
    }
}