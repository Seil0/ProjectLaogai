/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.controller.preferences

import android.content.Context
import android.graphics.Color
import org.mosad.seil0.projectlaogai.R
import org.mosad.seil0.projectlaogai.util.Course

/**
 * The PreferencesController class
 * contains all preferences and global variables that exist in this app
 */
object Preferences {

    var coursesCacheTime: Long = 0
    var mensaCacheTime: Long = 0
    var timetableCacheTime: Long = 0
    var gradesCacheTime: Long = 0
    var colorPrimary: Int = Color.parseColor("#009688")
    var colorAccent: Int = Color.parseColor("#0096ff")
    var gradesSyncInterval = 0
    var course = Course(
        "https://www.hs-offenburg.de/index.php?id=6627&class=class&iddV=DA64F6FE-9DDB-429E-A677-05D0D40CB636&week=0",
        "AI3"
    )
    var showBuffet = true
    var oGiants = false

    // TODO move!
    var themePrimary = 0
    var themeSecondary = 0

    // the save function
    fun save(context: Context) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        // save the update times (cache)
        with (sharedPref.edit()) {
            putLong(context.getString(R.string.save_key_coursesCacheTime),
                coursesCacheTime
            )
            putLong(context.getString(R.string.save_key_mensaCacheTime),
                mensaCacheTime
            )
            putLong(context.getString(R.string.save_key_timetableCacheTime),
                timetableCacheTime
            )
            putLong(context.getString(R.string.save_key_gradesCacheTime),
                gradesCacheTime
            )
            apply()
        }

    }

    /**
     * save the course locally
     */
    fun saveCourse(context: Context, course: Course) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        with (sharedPref.edit()) {
            putString(context.getString(R.string.save_key_course), course.courseName)
            putString(context.getString(R.string.save_key_courseTTLink), course.courseLink)
            apply()
        }

        this.course = course
    }

    /**
     * save the primary color
     */
    fun saveColorPrimary(context: Context, colorPrimary: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        with (sharedPref.edit()) {
            putInt(context.getString(R.string.save_key_colorPrimary),
                colorPrimary
            )
            apply()
        }

        this.colorPrimary = colorPrimary
    }

    /**
     * save the accent color
     */
    fun saveColorAccent(context: Context, colorAccent: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        with (sharedPref.edit()) {
            putInt(context.getString(R.string.save_key_colorAccent),
                colorAccent
            )
            apply()
        }

        this.colorAccent = colorAccent
    }

    fun saveGradesSync(context: Context, interval: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        with (sharedPref.edit()) {
            putInt(context.getString(R.string.save_key_gradesSyncInterval),
                interval
            )
            apply()
        }

        gradesSyncInterval = interval
    }

    /**
     * save showBuffet
     */
    fun saveShowBuffet(context: Context, showBuffet: Boolean) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        with (sharedPref.edit()) {
            putBoolean(context.getString(R.string.save_key_showBuffet),
                showBuffet
            )
            apply()
        }

        this.showBuffet = showBuffet
    }

    /**
     * initially load the stored values
     */
    fun load(context: Context) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        // load the update times (cache)
        coursesCacheTime = sharedPref.getLong(
            context.getString(R.string.save_key_coursesCacheTime),0
        )
        mensaCacheTime = sharedPref.getLong(
            context.getString(R.string.save_key_mensaCacheTime),0
        )
        timetableCacheTime = sharedPref.getLong(
            context.getString(R.string.save_key_timetableCacheTime),0
        )
        gradesCacheTime = sharedPref.getLong(
            context.getString(R.string.save_key_gradesCacheTime), 0
        )

        // load saved course
        course = Course(
            sharedPref.getString(context.getString(R.string.save_key_courseTTLink),"https://www.hs-offenburg.de/index.php?id=6627&class=class&iddV=DA64F6FE-9DDB-429E-A677-05D0D40CB636&week=0")!!,
            sharedPref.getString(context.getString(R.string.save_key_course), "AI3")!!
        )

        // load saved colors
        colorPrimary = sharedPref.getInt(
            context.getString(R.string.save_key_colorPrimary), colorPrimary
        )
        colorAccent = sharedPref.getInt(
            context.getString(R.string.save_key_colorAccent), colorAccent
        )

        // load grades sync interval
        gradesSyncInterval = sharedPref.getInt(
            context.getString(R.string.save_key_gradesSyncInterval), gradesSyncInterval
        )

        // load showBuffet
        showBuffet = sharedPref.getBoolean(
            context.getString(R.string.save_key_showBuffet), true
        )
    }

}