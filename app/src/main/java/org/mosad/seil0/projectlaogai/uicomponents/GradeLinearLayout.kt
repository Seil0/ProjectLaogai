/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.uicomponents

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import org.mosad.seil0.projectlaogai.databinding.LinearlayoutGradeBinding

class GradeLinearLayout(context: Context?): LinearLayout(context) {

    private val binding = LinearlayoutGradeBinding.inflate(LayoutInflater.from(context), this, true)

    var subjectName = ""
    var grade = ""
    var subSubjectName = ""
    var subGrade = ""

    fun set(func: GradeLinearLayout.() -> Unit): GradeLinearLayout = apply {
        func()

        binding.textSubject.text = subjectName
        binding.textGrade.text = grade
        binding.textSubSubject.text = subSubjectName
        binding.textSubGrade.text = subGrade
    }

    fun disableDivider() {
        binding.dividerGrade.visibility = View.GONE
    }

    fun disableSubSubject() {
        binding.linearSubSubject.visibility = View.GONE
    }
}