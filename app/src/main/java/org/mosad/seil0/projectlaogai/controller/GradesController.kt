/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.controller

import android.content.Context
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import org.mosad.seil0.projectlaogai.controller.preferences.Preferences
import org.mosad.seil0.projectlaogai.util.GradeSubject
import org.mosad.seil0.projectlaogai.worker.GradesUpdateWorker
import java.util.concurrent.TimeUnit

class GradesController {

    /**
     * show the difference between 2 grades sets
     */
    fun diffGrades(origMap: Map<String, ArrayList<GradeSubject>>, diffMap: Map<String, ArrayList<GradeSubject>>): ArrayList<GradeSubject> {
        val diff = ArrayList<GradeSubject>()

        diffMap.values.forEach { semester ->
            // if it's the same, no need to compare
            if (!origMap.containsValue(semester)) {
                semester.forEach { gradeSubject ->
                    // for each of the grades, check if it differs from the origMap

                    if (origMap.containsKey(gradeSubject.semester)) {
                        // a new or changed subject
                        if (gradeSubject !in origMap[gradeSubject.semester]!!) {
                            diff.add(gradeSubject)
                        }
                    } else {
                        // a new semester
                        diff.add(gradeSubject)
                    }
                }
            }
        }

        return diff
    }

    companion object {

        /**
         * stop the grades background sync, if the sync interval is greater than 0
         * start it again with the new interval
         */
        fun updateBackgroundSync(context: Context) {
            stopBackgroundSync(context)

            // if interval is not 0, start background Sync
            if (Preferences.gradesSyncInterval > 0)
                startBackgroundSync(context)
        }

        /**
         * start a new periodic worker GradesUpdateWorker
         */
        fun startBackgroundSync(context: Context) {
            val work = PeriodicWorkRequestBuilder<GradesUpdateWorker>(
                Preferences.gradesSyncInterval.toLong(),
                TimeUnit.HOURS
            ).build()

            val workManager = WorkManager.getInstance(context)
            workManager.enqueueUniquePeriodicWork("GradesUpdateWorker", ExistingPeriodicWorkPolicy.REPLACE, work)
        }

        /**
         * cancel GradesUpdateWorker
         */
        fun stopBackgroundSync(context: Context) {
            WorkManager.getInstance(context).cancelUniqueWork("GradesUpdateWorker")
        }

    }
}