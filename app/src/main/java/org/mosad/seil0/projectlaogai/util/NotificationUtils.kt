package org.mosad.seil0.projectlaogai.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.concurrent.atomic.AtomicInteger


class NotificationUtils(val context: Context) {

    companion object {
        const val CHANNEL_ID_GRADES = "channel_grades"

        val id = AtomicInteger(0)

        fun getId() = id.incrementAndGet()
    }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(CHANNEL_ID_GRADES, "Grades Channel", "A Channel")
        }
    }

    @RequiresApi(26)
    private fun createNotificationChannel(channelId: String, name: String, desc: String) {
        val mChannel = NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_DEFAULT)
        mChannel.description = desc

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(mChannel)
    }

}