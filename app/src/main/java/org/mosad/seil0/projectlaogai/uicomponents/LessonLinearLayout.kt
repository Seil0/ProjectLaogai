/**
 * ProjectLaogai
 *
 * Copyright 2019-2020  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.seil0.projectlaogai.uicomponents

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import org.mosad.seil0.projectlaogai.databinding.LinearlayoutLessonBinding
import org.mosad.seil0.projectlaogai.util.Lesson

class LessonLinearLayout(context: Context?) : LinearLayout(context) {

    private val binding = LinearlayoutLessonBinding.inflate(LayoutInflater.from(context), this, true)

    fun setLesson(lesson: Lesson, time: String) {
        binding.textLessonTime.text = time
        binding.textLessonSubject.text = lesson.lessonSubject
        binding.textLessonTeacher.text = lesson.lessonTeacher
        binding.textLessonRoom.text = lesson.lessonRoom
    }

    fun disableDivider() {
        binding.dividerLesson.visibility = View.GONE
    }

}